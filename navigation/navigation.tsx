import { NavigationContainer } from "@react-navigation/native";
import BottomTabNavigator from "./bottom-tab-navigator";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import AddExpenseScreen from "../screens/AddExpenseScreen";

export type RootStackParamList = {
  BottomTabNavigator: undefined;
  AddExpenseScreen: undefined;
};

const Stack = createNativeStackNavigator<RootStackParamList>();

export default function RootNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="BottomTabNavigator">
        <Stack.Screen
          name="BottomTabNavigator"
          component={BottomTabNavigator}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AddExpenseScreen"
          component={AddExpenseScreen}
          options={{
            presentation: "modal",
            title: "Add a new Expense",
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
