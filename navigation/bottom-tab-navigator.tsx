import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AllExpenses, RecentExpenses } from "../screens";

import { MaterialCommunityIcons, AntDesign } from "@expo/vector-icons";
import HeaderRightBtn from "../components/HeaderRightBtn";

const Tabs = createBottomTabNavigator();

export default function BottomTabNavigator() {
  return (
    <Tabs.Navigator
      initialRouteName="RecentExpenses"
      screenOptions={{
        headerRight: () => <HeaderRightBtn />,
        headerStyle: {},
        tabBarActiveTintColor: "#fc466b",
      }}
    >
      <Tabs.Screen
        name="RecentExpenses"
        component={RecentExpenses}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="timer-sand"
              color={color}
              size={size}
            />
          ),
          title: "Recent Expenses",
        }}
      />
      <Tabs.Screen
        name="AllExpenses"
        component={AllExpenses}
        options={{
          tabBarIcon: ({ color, size }) => (
            <AntDesign name="calendar" color={color} size={size} />
          ),
          title: "All Expenses",
        }}
      />
    </Tabs.Navigator>
  );
}
