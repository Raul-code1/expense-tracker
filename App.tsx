import "react-native-gesture-handler";

import { StatusBar } from "expo-status-bar";
import { Provider } from "react-redux";

import { store } from "./store/store";
import RootNavigation from "./navigation/navigation";

export default function App() {
  return (
    <Provider store={store}>
      <StatusBar style="auto" />
      <RootNavigation />
    </Provider>
  );
}
