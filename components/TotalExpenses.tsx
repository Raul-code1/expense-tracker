import { View, Text, StyleSheet } from "react-native";
import { formatter } from "../utils/currencyFormatter";

interface Props {
  total: number;
  howLongAgoText: string;
}

export default function TotalExpenses({ total, howLongAgoText }: Props) {
  return (
    <View style={styles.totalExpensesContainer}>
      <Text style={{ color: "white" }}>{howLongAgoText}</Text>
      <Text style={{ color: "white" }}>{formatter.format(total)}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  totalExpensesContainer: {
    backgroundColor: "black",
    alignSelf: "stretch",
    marginHorizontal: 20,
    borderRadius: 8,
    flexDirection: "row",
    height: 80,
    paddingHorizontal: 20,
    justifyContent: "space-between",
    alignItems: "center",
  },
});
