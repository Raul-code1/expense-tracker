import { Pressable, StyleSheet, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

import { AntDesign } from "@expo/vector-icons";
import { RootStackParamList } from "../navigation/navigation";

export default function HeaderRightBtn() {
  const navigation =
    useNavigation<NativeStackNavigationProp<RootStackParamList>>();

  function handlePress() {
    navigation.navigate("AddExpenseScreen");
  }

  return (
    <View style={styles.btnContainer}>
      <Pressable onPress={handlePress}>
        <AntDesign name="pluscircle" size={24} color="black" />
      </Pressable>
    </View>
  );
}
const styles = StyleSheet.create({
  btnContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
  },
});
