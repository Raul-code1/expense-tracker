import { View, Text } from "react-native";
import React from "react";
import { useAppSelector } from "../store/hooks/reduxHooks";
import { getContainerStyles } from "../styles/container";

interface Props {
  children: React.ReactNode;
}

export default function MainContainerLayout({ children }: Props) {
  const { theme } = useAppSelector((store) => store.theme);
  let screenContainer = getContainerStyles(theme);
  return <View style={screenContainer}>{children}</View>;
}
