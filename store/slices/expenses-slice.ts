import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { ExpensesState, SingleExpense } from "../../interfaces/Expenses";

const initialState: ExpensesState = {
  total: 0,
  totalRecentExpenses: 0,
  allExpenses: [],
  recentExpenses: [],
};

export const expensesSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    addExpense: (state, action: PayloadAction<SingleExpense>) => {
      state.allExpenses.push(action.payload);
    },
    getTotalExpenses: (state) => {
      state.total = state.allExpenses.reduce(
        (total, expense) => total + expense.amount,
        0
      );
    },
    getTotalRecentExpenses: (state) => {
      const currentDate = new Date();
      const sevenDaysAgo = new Date(currentDate);
      sevenDaysAgo.setDate(currentDate.getDate() - 7);

      state.recentExpenses = state.allExpenses.filter(
        (expense) => new Date(expense.date) >= sevenDaysAgo
      );

      state.totalRecentExpenses = state.allExpenses
        .filter((expense) => new Date(expense.date) >= sevenDaysAgo)
        .reduce((total, expense) => total + expense.amount, 0);
    },
  },
});

export const { addExpense, getTotalExpenses } = expensesSlice.actions;

export default expensesSlice;
