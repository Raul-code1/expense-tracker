import { configureStore } from "@reduxjs/toolkit";
import themeSlice from "./slices/theme-slice";
import expensesSlice from "./slices/expenses-slice";

export const store = configureStore({
  reducer: {
    theme: themeSlice.reducer,
    expenses: expensesSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
