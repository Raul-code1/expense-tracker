import MainContainerLayout from "../components/MainContainerLayout";
import { useAppSelector } from "../store/hooks/reduxHooks";
import TotalExpenses from "../components/TotalExpenses";

export default function AllExpenses() {
  const { total } = useAppSelector((store) => store.expenses);
  return (
    <MainContainerLayout>
      <TotalExpenses total={total} howLongAgoText="Total" />
    </MainContainerLayout>
  );
}
