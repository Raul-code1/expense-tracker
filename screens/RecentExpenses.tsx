import { useAppSelector } from "../store/hooks/reduxHooks";
import TotalExpenses from "../components/TotalExpenses";
import MainContainerLayout from "../components/MainContainerLayout";

export default function RecentExpenses() {
  const { totalRecentExpenses, recentExpenses } = useAppSelector(
    (store) => store.expenses
  );

  return (
    <MainContainerLayout>
      <TotalExpenses total={totalRecentExpenses} howLongAgoText="Last 7 days" />
    </MainContainerLayout>
  );
}
