import { themes } from "./theme";

export function getContainerStyles(theme: "light" | "dark") {
  let styles: any = {
    flex: 1,
    alignItems: "center",
    paddingTop: 20,
  };

  if (theme === "light") {
    return (styles = {
      ...styles,
      backgroundColor: themes.light.bgColor,
      textColor: themes.light.textColor,
    });
  } else if (theme === "dark") {
    return (styles = {
      ...styles,
      backgroundColor: themes.dark.bgColor,
      textColor: themes.dark.textColor,
    });
  }
}
