const colors = {
  mainWhite: "#ffffff",
  ligthGrey: "#b6b6b6",
  grey: "##8a8a8a",
  darkerGray: "#363636",
  black: "#000000",
};

export const themes = {
  light: {
    bgColor: colors.mainWhite,
    textColor: colors.black,
  },
  dark: {
    bgColor: colors.black,
    textColor: colors.mainWhite,
  },
};
