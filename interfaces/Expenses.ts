export interface ExpensesState {
  total: number;
  totalRecentExpenses: number;
  allExpenses: SingleExpense[];
  recentExpenses: SingleExpense[];
}

export interface SingleExpense {
  amount: number;
  date: Date;
  description: string;
  reason: string;
}
